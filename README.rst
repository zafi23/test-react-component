Toda la documentación para empezar está aquí:
https://dash.plotly.com/react-for-python-developers

Pasos seguidos:

1 -> crear entorno nuevo (lo he creado con poetry)
    poetry new test_react_compo

2 -> asegurarse de que phone sta instalado y NPM (si no esta instalado, instalarlo)

3 -> He instalado dash
    poetry add dash

4 -> dentro del entorno -> instalar cookiecutter
    poetry add cookiecutter

5 -> instalar yaml, pandas
    poetry add pyyaml
    poetry add pandas

Hacer build del proyecto y probar que va el codigo de ejemplo..
    ```
    npm run install
    npm run build
    ```
Probar el componente en el codigo de ejemplo.. se prueba en el usage.py
    python usage.py

Si todo va bien, deberia de salirte el componente de prueba

 Una vez probado esto.. para exportarlo:
    npm run build
    python setup.py sdist bdist_wheel
Esto último crea un .tar.gz en el directorio dist/

Para poder usar el componente en otro sitio hay que hacer el install del tar
 pip install my_dash_component.tar.gz
 poetry install my_dash_component.tar.gz


Se ha instalado el siguiente componenete de react --->
npm install react-nlp-annotate --save