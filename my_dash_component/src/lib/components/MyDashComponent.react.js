import React, {Component} from 'react';
import PropTypes from 'prop-types';
import NLPAnnotator from "react-nlp-annotate";



const labels = [
  {
    id: "gryffindor",
    displayName: "Gryffindor",
    description: "Daring, strong nerve and chivalry."
  },
  {
    id: "slytherin",
    displayName: "Slytherin",
    description: "Cunning and ambitious. Possibly dark wizard."
  }
];

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */
export default class MyDashComponent extends Component {
    render() {
        const {id, label, setProps, value} = this.props;

        return (
            <div id={id}>
                ExampleComponent: {label}&nbsp;
                <input
                    value={value}
                    onChange={
                        /*
                         * Send the new value to the parent component.
                         * setProps is a prop that is automatically supplied
                         * by dash's front-end ("dash-renderer").
                         * In a Dash app, this will update the component's
                         * props and send the data back to the Python Dash
                         * app server if a callback uses the modified prop as
                         * Input or State.
                         */
                        e => setProps({ value: e.target.value })
                    }
                />
                <NLPAnnotator
                      hotkeysEnabled
                      type="label-relationships"
                      labels={labels}
                      multipleLabels={false}
                      document="Harry was an honest to god good man"
                      onChange={(output) => {
                        console.log("Output is...", output);
                      }}
                      // this is just for label-relationships
                      entityLabels={labels}
                      relationshipLabels={labels}
                    />
            </div>

        );
    }
}

MyDashComponent.defaultProps = {};

MyDashComponent.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    /**
     * A label that will be printed when this component is rendered.
     */
    label: PropTypes.string.isRequired,

    /**
     * The value displayed in the input.
     */
    value: PropTypes.string,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
